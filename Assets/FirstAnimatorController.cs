using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class FirstAnimatorController : MonoBehaviour
{
    public Transform[] targetPositions; // Array holding the target positions
    public Animator animator; // Reference to the first animator

    public float walkingSpeed = 3.0f; // Speed value for walking

    private int currentTarget = 0;
    private NavMeshAgent agent;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.speed = walkingSpeed; // Set the walking speed
        MoveToNextTarget();
    }

    void Update()
    {
        if (!agent.pathPending && agent.remainingDistance < 0.1f)
        {
            if (currentTarget == targetPositions.Length - 1)
            {
                animator.SetBool("isWalking", true);
            }

            MoveToNextTarget();
        }
    }

    void MoveToNextTarget()
    {
        if (currentTarget >= targetPositions.Length)
        {
            currentTarget = 0; // Reset to the beginning
        }

        agent.SetDestination(targetPositions[currentTarget].position);
        currentTarget++;
    }
}
