using UnityEngine;

public class VRMenuInstantiation : MonoBehaviour
{
    public GameObject cpu;
    public GameObject gpu;
    public GameObject motherboard;
    public GameObject casee;
    public GameObject Desk; // Reference to the table object

    // Functions to handle button clicks
    public void OnCPUPressed()
    {
        // Adjust spawn position for CPU prefab
        Vector3 spawnPosition = new Vector3(2.637f, 1.716f, -2.1f);
        Instantiate(cpu, spawnPosition, Quaternion.identity);
    }

    public void OnGPUPressed()
    {
        // Adjust spawn position for GPU prefab
        Vector3 spawnPosition = new Vector3(3.09f,1.83f, -2.1f);
        Instantiate(gpu, spawnPosition, Quaternion.identity);
    }

    public void OnMotherboardPressed()
    {
        // Adjust spawn position for motherboard prefab
        Vector3 spawnPosition = new Vector3(3f, 1.76f, -2.1f);
        Instantiate(motherboard, spawnPosition, Quaternion.identity);
    }

    public void OnPCCasePressed()
    {
        // Adjust spawn position for PC case prefab
        Vector3 spawnPosition = new Vector3(-0.43f, 0.919f, -2.32f);
        Instantiate(casee, spawnPosition, Quaternion.identity);
    }
}
