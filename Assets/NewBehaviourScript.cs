using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class CustomInteraction : MonoBehaviour
{
    private XRGrabInteractable grabInteractable;
    public string grabbableTag = "Grabbable"; // Set the tag for objects that can be grabbed

    private void Start()
    {
        grabInteractable = GetComponent<XRGrabInteractable>();

        if (grabInteractable != null)
        {
            grabInteractable.onSelectEntered.AddListener(OnGrab);
            grabInteractable.onSelectExited.AddListener(OnRelease);
        }
    }

    private void OnGrab(XRBaseInteractor interactor)
    {
        if (CanGrab())
        {
            Debug.Log("Object grabbed!");
            // Perform specific actions upon grabbing
        }
    }

    private void OnRelease(XRBaseInteractor interactor)
    {
        Debug.Log("Object released!");
        // Perform specific actions upon releasing
    }

    private bool CanGrab()
    {
        // Check conditions for allowing grabbing
        if (gameObject.CompareTag(grabbableTag))
        {
            return true; // Allow grabbing if the object has the grabbable tag
        }
        // Add more conditions here based on your custom logic (e.g., specific layers, properties, etc.)

        return false; // Prevent grabbing if it doesn't meet the conditions
    }
}
