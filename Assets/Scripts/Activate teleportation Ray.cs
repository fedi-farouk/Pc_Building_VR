using System.Collections;
using System.Collections.Generic;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine;
using UnityEngine.InputSystem;

public class ActivateteleportationRay : MonoBehaviour
{
    public GameObject leftTeleportation;
    public GameObject rightTeleportation;
    public InputActionProperty leftActive;
    public InputActionProperty rightActive;
    public InputActionProperty leftCancel;
    public InputActionProperty rightCancel;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        leftTeleportation.SetActive(leftCancel.action.ReadValue<float>() ==0 &&  leftActive.action.ReadValue<float>() > 0.1f);
        rightTeleportation.SetActive(rightCancel.action.ReadValue<float>() == 0 && rightActive.action.ReadValue<float>() > 0.1f);

    }
}
