using UnityEngine;

public class AnimationController : MonoBehaviour
{
    public GameObject[] objectsToAnimate; // List of GameObjects to animate

    public void OnButtonClick()
    {
        // Start the Animator component for each object
        foreach (GameObject obj in objectsToAnimate)
        {
            if (obj != null)
            {
                Animator animator = obj.GetComponent<Animator>();
                if (animator != null)
                {
                    animator.enabled = true; // Ensure animator is enabled
                }
            }
        }
    }
}
