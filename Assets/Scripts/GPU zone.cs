using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class GPUzone : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        // V�rifie si l'objet en collision a le tag "GPU"
        if (other.CompareTag("GPU"))
        {
            // D�sactive l'objet
            other.gameObject.SetActive(false);

            // Appelle une fonction ou d�clenche une action pour g�rer la capture de l'objet
            HandleCapturedGPUObject(other.gameObject);
        }
    }

    private void HandleCapturedGPUObject(GameObject gpuObject)
    {
        // Ici, tu peux impl�menter la logique pour g�rer l'objet captur�
        // Peut-�tre ajouter l'objet � un inventaire, comptabiliser le nombre d'objets captur�s, etc.
        Debug.Log("Objet GPU captur� !");
    }
}
