using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RGB : MonoBehaviour
{
    public float speed = 1.0f; // Speed of color change
    private Renderer rend;
    private float timeCounter = 0.0f;

    void Start()
    {
        rend = GetComponent<Renderer>();
    }

    void Update()
    {
        // Calculate RGB values based on time and speed
        float r = Mathf.Sin(speed * timeCounter) * 0.5f + 0.5f;
        float g = Mathf.Sin(speed * timeCounter + 2.0f) * 0.5f + 0.5f;
        float b = Mathf.Sin(speed * timeCounter + 4.0f) * 0.5f + 0.5f;

        // Set the object's color
        rend.material.color = new Color(r, g, b);

        // Increment time counter
        timeCounter += Time.deltaTime;
    }
}
