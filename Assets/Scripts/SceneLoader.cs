
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public void LoadReparation()
    {
        SceneManager.LoadScene("Reparation");
    }

    public void Loadassemblage()
    {
        SceneManager.LoadScene("Assemblage");
    }

    public void Loadunboxing()
    {
        SceneManager.LoadScene("unboxing");
    }

}