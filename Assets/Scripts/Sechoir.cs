using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sechoir : MonoBehaviour
{
    private AudioSource audioSource;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.Stop(); // Assure que le son ne joue pas au d�but
    }

    // Appel� lorsqu'un objet entre en collision avec le s�che-cheveux
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Direct Interactor")) // V�rifie s'il s'agit de la main de l'avatar
        {
            audioSource.Play(); // Active le son lorsque la main touche le s�che-cheveux
        }
    }

    // Appel� lorsqu'un objet quitte la collision avec le s�che-cheveux
    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Direct Interactor")) // V�rifie si c'est la main de l'avatar qui quitte
        {
            audioSource.Stop(); // Arr�te le son lorsque la main quitte le s�che-cheveux
        }
    }
}
