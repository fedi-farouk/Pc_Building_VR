using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spill : MonoBehaviour
{
    public ParticleSystem liquidFlow;
    private bool isFlipped;
    private float flipThreshold = 90.0f; // Change this value to adjust sensitivity

    void Update()
    {
        // Check if the tube is flipped based on its x-axis rotation
        isFlipped = transform.localRotation.eulerAngles.x > flipThreshold;

        // Activate or deactivate the particle system based on the flipped state
        liquidFlow.startLifetime = isFlipped ? 1.0f : 0.0f;
    }
}