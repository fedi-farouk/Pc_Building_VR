using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisTourne : MonoBehaviour
{
    public float rotationSpeed = 50f; // Vitesse de rotation du tournevis
    private bool isRotating = false;

    private void OnCollisionEnter(Collision collision)
    {
        // V�rifie si le tournevis entre en collision avec un objet portant le tag "Vis"
        if (collision.gameObject.CompareTag("TourneVis")) // "Vis" est le tag attribu� � l'objet tournevis
        {
            isRotating = true;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        // Arr�te la rotation lorsque le tournevis n'est plus en collision avec l'objet "Vis"
        if (collision.gameObject.CompareTag("TourneVis"))
        {
            isRotating = false;
        }
    }

    void Update()
    {
        // Si le tournevis est en contact avec l'objet "Vis", le fait tourner
        if (isRotating)
        {
            // Rotation du tournevis sur l'axe Y
            transform.Rotate(Vector3.forward, rotationSpeed * Time.deltaTime);
        }
    }
}
