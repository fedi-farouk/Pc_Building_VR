using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ui : MonoBehaviour
{
    public GameObject panelToHide;
    public GameObject panelToShow;
    public void Restart()
    {
        SceneManager.LoadScene("client");  
    }
    public void SwitchPanels()
    {
        if (panelToHide != null)
        {
            panelToHide.SetActive(false); // Hide the panel
        }

        if (panelToShow != null)
        {
            panelToShow.SetActive(true); // Show the other panel
        }
    }

}
