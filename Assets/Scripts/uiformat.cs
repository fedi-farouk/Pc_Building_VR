using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class uiformat : MonoBehaviour
{
    public GameObject objectToShow; // Reference to the object you want to appear
    public GameObject initialUI; // Reference to the initial UI GameObject
    public GameObject secondUI; // Reference to the second UI GameObject

    // Start is called when the script is initialized
    void Start()
    {
        // Hide the second UI at the start
        if (secondUI != null)
        {
            secondUI.SetActive(false);
        }
    }

    // This method will be called when the button is clicked
    public void OnButtonClick()
    {
        if (objectToShow != null)
        {
            objectToShow.SetActive(true); // Set the object to be active/enabled
        }

        if (initialUI != null)
        {
            initialUI.SetActive(false); // Hide the initial UI
        }

        StartCoroutine(ShowSecondUIAfterDelay());
    }

    IEnumerator ShowSecondUIAfterDelay()
    {
        yield return new WaitForSeconds(45f); // Wait for 45 seconds

        if (secondUI != null)
        {
            secondUI.SetActive(true); // Show the second UI after the delay
        }
    }
}
