using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class uiparts : MonoBehaviour
{
    public GameObject psuPrefab; // Assign the PSU prefab in the Inspector
    public GameObject gpuPrefab; // Assign the GPU prefab in the Inspector
    public GameObject cpuPrefab; // Assign the CPU prefab in the Inspector
    public GameObject fanPrefab; // Assign the Fan prefab in the Inspector

    public GameObject psuPosition; // Empty GameObject for PSU position
    public GameObject gpuPosition; // Empty GameObject for GPU position
    public GameObject cpuPosition; // Empty GameObject for CPU position
    public GameObject fanPosition; // Empty GameObject for Fan position

    public GameObject panel; // Assign the panel in the Inspector

    private bool panelVisible = false;

    public void TogglePanel()
    {
        panelVisible = !panelVisible;
        panel.SetActive(panelVisible);
    }

    public void InstantiatePSU()
    {
        Instantiate(psuPrefab, psuPosition.transform.position, Quaternion.identity);
    }

    public void InstantiateGPU()
    {
        Instantiate(gpuPrefab, gpuPosition.transform.position, Quaternion.identity);
    }

    public void InstantiateCPU()
    {
        Instantiate(cpuPrefab, cpuPosition.transform.position, Quaternion.identity);
    }

    public void InstantiateFan()
    {
        Instantiate(fanPrefab, fanPosition.transform.position, Quaternion.identity);
    }
}
