using System.Collections;
using UnityEngine;

public class SecondAnimatorController : MonoBehaviour
{
    public Animator animator; // Reference to the second animator

    void Start()
    {
        // Your initialization logic, if needed
    }

    void Update()
    {
        // Your update logic, if needed
    }

    public void StartWalking()
    {
        // Add any logic specific to the second animator starting to walk
        animator.SetBool("isWalking", true);
    }

    public void StartTalking()
    {
        // Add any logic specific to the second animator starting to talk
        animator.SetBool("isTalking", true);
    }
}
