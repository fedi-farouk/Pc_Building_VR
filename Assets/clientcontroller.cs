using UnityEngine;
using UnityEngine.AI;
using System.Collections;

public class clientcontroller : MonoBehaviour
{

    public Animator animator;
    public NavMeshAgent agent;
    public Transform targetPosition;

    private bool isWalking = false;
    private bool isTalking = false;

    void Start()
    {
        animator = GetComponent<Animator>();
        if (animator == null)
        {
            Debug.LogError("Animator component not found on the character!");
        }

        agent = GetComponent<NavMeshAgent>();
        if (agent == null)
        {
            Debug.LogError("NavMeshAgent component not found on the character!");
        }
        agent.updateRotation = false; // Character uses animation rotations

        // Start the walking sequence
        StartCoroutine(StartWalkingSequence());
    }

    void Update()
    {
        if (isWalking && !isTalking)
        {
            // Check if walking duration has passed
            if (Time.time > startTime + 2f)
            {
                isWalking = false;
                isTalking = true;
                UpdateAnimationStates();
            }
        }
    }

    private float startTime;

    private IEnumerator StartWalkingSequence()
    {
        // Wait for 2 seconds before starting to walk
        yield return new WaitForSeconds(2f);

        // Set the destination and start walking
        agent.SetDestination(targetPosition.position);
        isWalking = true;
        startTime = Time.time; // Record the start time
        UpdateAnimationStates();
    }

    private void UpdateAnimationStates()
    {
        // Update animator parameters based on current state
        animator.SetBool("isWalking", isWalking);
        animator.SetBool("isTalking", isTalking);
    }

    private void OnCollisionEnter(Collision collision)
    {
        // Check if the character collides with a cube
        if (collision.gameObject.CompareTag("CubeTag"))
        {
            // Stop walking animation or perform other actions as needed
            agent.isStopped = true;
            isWalking = false;
            UpdateAnimationStates();
        }
    }
}
