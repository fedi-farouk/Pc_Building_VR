using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class instensiateIA : MonoBehaviour
{
    public GameObject[] panels;
    public Transform character; // Reference to the character's transform
    public float activationDistance = 5f; // Define the distance for activation

    bool panelsActivated = false;

    void Start()
    {
        // Disable all panels initially
        DisableAllPanels();
    }

    void Update()
    {
        // Check if the character is within activation distance of the canvas
        if (!panelsActivated && Vector3.Distance(transform.position, character.position) < activationDistance)
        {
            // Display a random panel when the character is close enough to the canvas
            ActivateRandomPanel();
            panelsActivated = true; // Set flag to prevent continuous activation
        }
    }

    void DisableAllPanels()
    {
        foreach (GameObject panel in panels)
        {
            panel.SetActive(false);
        }
    }

    void ActivateRandomPanel()
    {
        int randomIndex = Random.Range(0, panels.Length);
        panels[randomIndex].SetActive(true);
    }
}
