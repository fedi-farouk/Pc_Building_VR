using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movement : MonoBehaviour
{
    public Transform[] targetPositions; // Array holding the target positions
    public Animator animator; // Reference to the character's animator

    public float walkingSpeed = 3.0f; // Speed value for walking

    private int currentTarget = 0;
    private UnityEngine.AI.NavMeshAgent agent;

    void Start()
    {
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        agent.speed = walkingSpeed; // Set the walking speed
        MoveToNextTarget();
    }

    void Update()
    {
        if (!agent.pathPending && agent.remainingDistance < 0.1f)
        {
            if (currentTarget == 0)
            {
                animator.SetBool("isWalking", false);
                animator.SetBool("isTalking", false);
                Invoke("StartWalking", 4f);
            }
            else if (currentTarget == 3)
            {
                animator.SetBool("isWalking", false);
                animator.SetBool("isTalking", true);
            }
            else
            {
                MoveToNextTarget();
            }
        }
    }

    void MoveToNextTarget()
    {
        if (currentTarget >= targetPositions.Length)
        {
            currentTarget = 0; // Reset to the beginning
        }

        agent.SetDestination(targetPositions[currentTarget].position);
        currentTarget++;
        animator.SetBool("isWalking", true);
        animator.SetBool("isTalking", false);
    }

    void StartWalking()
    {
        MoveToNextTarget();
    }
}